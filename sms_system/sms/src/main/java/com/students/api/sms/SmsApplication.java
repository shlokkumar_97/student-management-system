package com.students.api.sms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.students.api.sms.entity.Student;
import com.students.api.sms.repository.StudentRepository;

@SpringBootApplication
public class SmsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SmsApplication.class, args);
	}

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
//		Student student1 = new Student("Shlok", "Kumar", "shlok@test.com");
//		studentRepository.save(student1);
//		
//		Student student2 = new Student("Hello", "Hi", "kumar@test.com");
//		studentRepository.save(student2);
//		
//		Student student3 = new Student("Bye", "Lol", "Lol@test.com");
//		studentRepository.save(student3);

	}

}
